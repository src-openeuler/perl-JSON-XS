Name:           perl-JSON-XS
Summary:        JSON serializing/de-serializing, done correctly and fast
Epoch:          1
Version:        4.03
Release:        2
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
Group:          Development/Libraries
URL:            https://metacpan.org/release/JSON-XS
Source0:        https://cpan.metacpan.org/authors/id/M/ML/MLEHMANN/JSON-XS-%{version}.tar.gz

BuildRequires:  coreutils gcc make perl-devel perl-generators perl-interpreter
BuildRequires:  perl(Canary::Stability) perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires:  sed perl(common::sense) perl(Exporter) perl(Types::Serialiser)
BuildRequires:  perl(XSLoader) perl(Getopt::Long) perl(Storable) perl(strict)
BuildRequires:  perl(Data::Dumper) perl(Encode) perl(Test) perl(Test::More)
BuildRequires:  perl(Tie::Array) perl(Tie::Hash) perl(utf8) perl(warnings)


%{?perl_default_filter}

%description
This module converts Perl data structures to JSON and vice versa. Its
primary goal is to be correct and its secondary goal is to be fast. To
reach the latter goal it was written in C.

%package_help

%prep
%autosetup -n JSON-XS-%{version}

sed -i 's/\r//' t/*
perl -pi -e 's|^#!/opt/bin/perl|#!%{__perl}|' eg/*
chmod -c -x eg/*

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PERLLOCAL=1 NO_PACKLIST=1
%make_build

%install
%make_install
%{_fixperms} -c %{buildroot}

%check
make test

%files
%doc Changes README eg/
%license COPYING
%{perl_vendorarch}/*
%exclude %dir %{perl_vendorarch}/auto
%{_bindir}/*

%files help
%{_mandir}/man[13]/*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 1:4.03-2
- drop useless perl(:MODULE_COMPAT) requirement

* Fri Jan 29 2021 yuanxin <yuanxin24@huawei.com> - 1:4.03-1
- upgrade version to 4.03

* Thu Mar 5 2020 openEuler Buildteam <buildteam@openeuler.org> - 1:3.04-6
- Package init
